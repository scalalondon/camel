package uk.co.techneurons


package uk.co.techneurons

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import org.apache.camel.Endpoint
import org.apache.camel.EndpointInject
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.direct.DirectComponent
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.test.junit4.CamelTestSupport
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.IOException

/**
 * @author Stephen Samuel
 */
object ConsumerIntTest {
  private val logger: Logger = LoggerFactory.getLogger(classOf[ConsumerIntTest])
  private val EXCHANGE: String = "ex1"
}

class ConsumerIntTest extends CamelTestSupport {

  val to: MockEndpoint = new MockEndpoint("mock:result", new DirectComponent())

  @Override
  @throws(classOf[Exception])
  override def createRouteBuilder: RouteBuilder = {
    return new RouteBuilder() {
      @Override
      @throws(classOf[Exception])
      def configure {

        from("rabbitmq:localhost:5672/" + ConsumerIntTest.EXCHANGE + "?username=cameltest&password=cameltest").to(to)
      }
    }
  }

  @Test
  @throws(classOf[InterruptedException])
  @throws(classOf[IOException])
  def sentMessageIsReceived {

   to.expectedMessageCount(1)

    val factory: ConnectionFactory = new ConnectionFactory();
    factory.setHost("localhost")
    factory.setPort(5672)
    factory.setUsername("cameltest")
    factory.setPassword("cameltest")
    factory.setVirtualHost("/")
    val conn: Connection = factory.newConnection();

    val properties: AMQP.BasicProperties.Builder = new AMQP.BasicProperties.Builder();

    val channel: Channel = conn.createChannel();

    channel.basicPublish(ConsumerIntTest.EXCHANGE, "", properties.build, "hello world".getBytes)
    to.assertIsSatisfied
  }

}